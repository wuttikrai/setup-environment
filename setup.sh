if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

APPLICATION_PATH="$(dirname $0)/applications"
PREFIX_PATH="http://ubuntu:@192.168.3.22/new_ubuntu"

export ATOM_DOWNLOAD_PATH="$PREFIX_PATH/atom-amd64.deb"
export INSOMNIA_DOWNLOAD_PATH="$PREFIX_PATH/insomnia.deb"
export INTELLIJ_DOWNLOAD_PATH="$PREFIX_PATH/intellij.tar.gz"
export ROBOMONGO_DOWNLOAD_PATH="$PREFIX_PATH/robomongo.tar.gz"
export SCALA_DOWNLOAD_PATH="$PREFIX_PATH/scala-2.11.8.deb"
export SLACK_DOWNLOAD_PATH="$PREFIX_PATH/slack.deb"
export SMARTGIT_DOWNLOAD_PATH="$PREFIX_PATH/smartgit.deb"
export SUBLIME_DOWNLOAD_PATH="$PREFIX_PATH/sublime.deb"
export TWEAK_DOWNLOAD_PATH="$PREFIX_PATH/tweak.deb"
export ANDROID_STUDIO_DOWNLOAD_PATH="$PREFIX_PATH/android-studio.tar.gz"
export HIPCHAT_DOWNLOAD_PATH="$PREFIX_PATH/hipchat.deb"

echo "
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        ======== SELECT PACKAGES ========

  Group packages
    [b]  Backend Developer (1 2 3 4 6 7 8 9 10 23)

  Packages
    [0]  Select all
    [1]  Atom Editor
    [2]  Google Chrome
    [3]  IntelliJ IDEA 2016
    [4]  Slack
    [5]  Sublime
    [6]  SmartGit
    [7]  Firefox
    [8]  SBT
    [9]  Scala
    [10] Java SDK 8
    [11] VLC Player
    [12] MongoDB
    [13] Robomongo
    [14] Ubuntu Theme
    [15] Ubuntu Tweak
    [16] Docker
    [17] Docker-Compose
    [18] Docker-Riak-CS
    [19] Insomnia (Application for test API)
    [20] NodeJS v6.x
    [21] React Navive and Babel CLI
    [22] Android Studio
    [23] HipChat

  Command
    [x]  Exit

Example: 1 2 4 10 11 OR Example: b 12 13

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
"

read -p "Enter: " INPUT </dev/tty

if [ -z "$INPUT" ]; then exit 1; fi

for str in $INPUT
do
  case "$str" in
    "x") exit 1 ;;
  esac
done

# sh "$APPLICATION_PATH/optimization.sh"
sh "$APPLICATION_PATH/alacarte.sh"
sh "$APPLICATION_PATH/git.sh"
sh "$APPLICATION_PATH/vim.sh"

packages() {
  for n in $*
  do
    case "$n" in
      "1") sh "$APPLICATION_PATH/atom-editor.sh" ;;
      "2") sh "$APPLICATION_PATH/google-chrome.sh" ;;
      "3") sh "$APPLICATION_PATH/intellij.sh" ;;
      "4") sh "$APPLICATION_PATH/slack.sh" ;;
      "5") sh "$APPLICATION_PATH/sublime.sh" ;;
      "6") sh "$APPLICATION_PATH/smartgit.sh" ;;
      "7") sh "$APPLICATION_PATH/firefox.sh" ;;
      "8") sh "$APPLICATION_PATH/sbt.sh" ;;
      "9") sh "$APPLICATION_PATH/scala.sh" ;;
      "10") sh "$APPLICATION_PATH/java-sdk-8.sh" ;;
      "11") sh "$APPLICATION_PATH/vlc-player.sh" ;;
      "12") sh "$APPLICATION_PATH/mongodb.sh" ;;
      "13") sh "$APPLICATION_PATH/robomongo.sh" ;;
      "14") sh "$APPLICATION_PATH/ubuntu-theme.sh" ;;
      "15") sh "$APPLICATION_PATH/ubuntu-tweak.sh" ;;
      "16") sh "$APPLICATION_PATH/docker.sh" ;;
      "17") sh "$APPLICATION_PATH/docker-compose.sh" ;;
      "18") sh "$APPLICATION_PATH/docker-riak-cs.sh" ;;
      "19") sh "$APPLICATION_PATH/insomnia.sh" ;;
      "20") sh "$APPLICATION_PATH/nodejs.sh" ;;
      "21") sh "$APPLICATION_PATH/react-native.sh" ;;
      "22") sh "$APPLICATION_PATH/android-studio.sh" ;;
      "23") sh "$APPLICATION_PATH/hipchat.sh" ;;
    esac
  done
}

for str in $INPUT
do
  case "$str" in
    "0") packages 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 ;;
    "b") packages 1 2 3 4 6 7 8 9 10 23 ;;
    *) packages $str ;;
  esac
done

# sh "$APPLICATION_PATH/app-launcher.sh"
