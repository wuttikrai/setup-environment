#!/bin/sh

if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

GIT=$(git | grep -E "^usage:\ git")
SETUP_PATH="/tmp/setup-environment"
CURRENT_USER=$(sudo echo "$HOME" | sed -e "s/\/home\//""/")

if [ -z "$GIT" ]; then
  sudo apt-get update &&
  sudo apt-get install -y git
fi &&

cd /tmp/
sudo rm -rf $SETUP_PATH/ &&
sudo git clone https://wuttikrai@bitbucket.org/wuttikrai/setup-environment.git &&
sudo chown -R $CURRENT_USER:$CURRENT_USER /tmp/setup-environment/ &&
sudo rm -rf $SETUP_PATH/.git/ &&

sudo sh $SETUP_PATH/setup.sh
