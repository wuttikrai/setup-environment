if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

ROBOMONGO=$(ls /opt/ | grep robomongo)
DEB_FILE_NAME="install-robomongo.tar.gz"
DEB_PATH="/tmp/$DEB_FILE_NAME"
EXIST=$(ls /tmp/ | grep $DEB_FILE_NAME)
APPLICATION_PATH="$(dirname $0)"
CURRENT_USER=$(sudo echo "$HOME" | sed -e "s/\/home\//""/")

ROBOMONGO_DESKTOP="
[Desktop Entry]\n
Name=Robomongo\n
Comment=Management MongoDB\n
Exec=/opt/robomongo/bin/robomongo\n
Terminal=false\n
Type=Application\n
StartupNotify=true\n
Icon=/opt/robomongo/bin/robomongo.png\n
Categories=Development;Debugger;Profiling;
"

if [ -z "$ROBOMONGO" ]; then
  if [ -z "$EXIST" ]; then
    wget -O $DEB_PATH $ROBOMONGO_DOWNLOAD_PATH
  fi

  cd /tmp/
  sudo rm -rf /tmp/robomongo/ /opt/robomongo/ &&
  sudo chown -R $CURRENT_USER:$CURRENT_USER $DEB_PATH &&
  sudo tar xvfz $DEB_PATH &&
  sudo mv /tmp/robomongo* /tmp/robomongo/ &&
  sudo mv /tmp/robomongo/ /opt/ &&
  sudo chown -R $CURRENT_USER:$CURRENT_USER /opt/ &&
  sudo cp "$APPLICATION_PATH/icons/robomongo.png" /opt/robomongo/bin/ &&

  sudo rm -rf /usr/share/applications/robomongo.desktop ~/.local/share/applications/robomongo.desktop &&

  echo $ROBOMONGO_DESKTOP > /usr/share/applications/robomongo.desktop &&
  echo $ROBOMONGO_DESKTOP > ~/.local/share/applications/robomongo.desktop &&

  sudo update-desktop-database /usr/share/applications/ &&
  sudo update-desktop-database ~/.local/share/applications/
fi
