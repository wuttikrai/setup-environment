if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

SCALA=$(scala -version 2>&1 | grep -E "^Scala code runner version.+")
DEB_FILE_NAME="scala.deb"
EXIST=$(ls /tmp/ | grep $DEB_FILE_NAME)
DEB_PATH="/tmp/$DEB_FILE_NAME"

if [ -z "$SCALA" ]; then
  if [ -z "$EXIST" ]; then
    wget -O $DEB_PATH $SCALA_DOWNLOAD_PATH
  fi

  sudo dpkg -i $DEB_PATH
  sudo apt-get -f install -y
fi
