if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

dependency() {
  sh "$(dirname $0)/docker.sh" &&

  S3CMD=$(s3cmd -v | grep -E "Make bucket")
  if [ -z "$S3CMD" ]; then sudo apt-get install -y s3cmd; fi
}

make_build() {
  cd ~
  sudo rm -rf ~/$1/
  git clone https://github.com/hectcastro/$1.git &&
  sudo chown -R $SUDO_USER:$SUDO_USER ~/$1/ &&
  cd ~/$1/
  echo "running make build $1..."
  make build &&
  echo " done."
}

make_cluster() {
  DOCKER_RIAK_CS_HAPROXY=1 DOCKER_RIAK_CS_AUTOMATIC_CLUSTERING=1 DOCKER_RIAK_CS_CLUSTER_SIZE=5 make start-cluster ~/docker-riak-cs/bin/start-cluster.sh
}

get_key() {
  ssh -i "${INSECURE_KEY_FILE}" -o "LogLevel=quiet" -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
  -p "${CS01_PORT}" "root@${CLEAN_DOCKER_HOST}" egrep "$1" /etc/riak-cs/app.config | cut -d'"' -f2
}

set_config() {
  config=$(echo "$line" | grep -E "$1 =")
  if [ -n "$config" ]; then
    line="$1 = $2"
  fi
}

auto_config_s3() {
  DOCKER_HOST="localhost"
  CLEAN_DOCKER_HOST=$(echo "${DOCKER_HOST}" | cut -d'/' -f3 | cut -d':' -f1)
  CLEAN_DOCKER_HOST=${CLEAN_DOCKER_HOST:-localhost}

  INSECURE_KEY_FILE=~/docker-riak-cs/.insecure_key
  CS01_PORT=$(docker port riak-cs01 22 | cut -d':' -f2)

  if [ -f $INSECURE_KEY_FILE ]; then
    chmod 600 ~/docker-riak-cs/.insecure_key

    ACCESS_KEY=$(get_key admin_key)
    SECRET_KEY=$(get_key admin_secret)
  fi

  TEMP_FILE=/tmp/temp_file_s3cfg
  echo -n "" > $TEMP_FILE

  while IFS='' read -r line || [ -n "$line" ]; do
    set_config "access_key" "$ACCESS_KEY"
    set_config "secret_key" "$SECRET_KEY"
    set_config "proxy_host" "localhost"
    set_config "proxy_port" "8080"
    set_config "use_https" "False"

    echo "$line" >> $TEMP_FILE
  done < "$(echo ~/.s3cfg)"

  cat $TEMP_FILE > ~/.s3cfg
}

install() {
  dependency
  make_build docker-riak-cs-haproxy
  make_build docker-riak-cs
  make_cluster
  auto_config_s3
}

install
