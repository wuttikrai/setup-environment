#! /bin/bash

LAUNCH=$(find /tmp/setup-environment/ -name "app-launcher.sh" | head -1)
if [ -z "$LAUNCH" ]; then
  LAUNCH=$(find /home/ -name "app-launcher.sh" | head -1)
fi

if [ -z "$LAUNCH" ]; then echo "Can't modify launcher."; else
  if [ $(id -u) -eq 0 ]; then
    echo "sh $LAUNCH;clear" | xclip -selection clipboard
    echo "Set launcher: ctrl+shift+v"
    exit 1
  fi

  DEFAULT="[
    'application://org.gnome.Nautilus.desktop',
    'unity://running-apps',
    'application://syntevo-smartgit.desktop',
    'application://chrome-hgmloofddffdnphfgcellkdfbfbjeloo-Default.desktop',
    'application://google-chrome.desktop',
    'application://atom.desktop',
    'application://gnome-terminal.desktop',
    'application://slack.desktop',
    'application://intellij.desktop',
    'application://robomongo.desktop',
    'application://vlc.desktop',
    'unity://expo-icon',
    'unity://devices'
  ]"

  gsettings set com.canonical.Unity.Launcher favorites "$DEFAULT"
fi
