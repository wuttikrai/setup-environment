if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

sudo updatedb
INSOMNIA=$(locate insomnia | grep -E "^/usr/share/doc/insomnia$")
DEB_FILE_NAME="insomnia.deb"
EXIST=$(ls /tmp/ | grep $DEB_FILE_NAME)
DEB_PATH="/tmp/$DEB_FILE_NAME"

if [ -z "$INSOMNIA" ]; then
  if [ -z "$EXIST" ]; then
    wget -O $DEB_PATH $INSOMNIA_DOWNLOAD_PATH
  fi

  sudo dpkg -i $DEB_PATH
  sudo apt-get -f install -y
fi
