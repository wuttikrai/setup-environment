if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

ALACARTE=$(alacarte -version 2>&1 | grep -E "File\ \"/usr/bin/alacarte\".+")

if [ -z "$ALACARTE" ]; then
  echo -n "running apt-get update..." && sudo apt-get update -y -qq && echo " done."
  sudo apt-get install -y alacarte
fi
