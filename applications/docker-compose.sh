if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

D_COMPOSE=$(docker-compose --version | grep -E "docker-compose version.+, build.+")

if [ -z "$D_COMPOSE" ]; then
  sudo curl -L https://github.com/docker/compose/releases/download/1.8.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose;
  sudo chmod +x /usr/local/bin/docker-compose;
fi
