if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

JAVA=$(java -version 2>&1 | grep -E "java version \"1.8.+")

if [ -z "$JAVA" ]; then
  yes "" | sudo add-apt-repository ppa:webupd8team/java
  echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections &&
  echo -n "running apt-get update..." && sudo apt-get update -y -qq && echo " done."
  sudo apt-get install -y oracle-java8-installer
  sudo apt-get -f install -y
  sudo apt-get install -y oracle-java8-set-default
fi
