if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

FIREFOX=$(firefox --version | grep -E "^Mozilla\ Firefox\ .+")

if [ -z "$FIREFOX" ]; then
  yes "" | sudo add-apt-repository ppa:ubuntu-mozilla-daily/ppa
  echo -n "running apt-get update..." && sudo apt-get update -y -qq && echo " done."
  sudo apt-get install -y firefox
fi
