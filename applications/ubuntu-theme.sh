if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

sudo updatedb
THEME=$(locate numix-gtk-theme | grep /usr/share/doc/numix-)

if [ -z "$THEME" ]; then
  # Add repository
  yes "" | sudo add-apt-repository ppa:numix/ppa
  yes "" | sudo add-apt-repository ppa:moka/daily
  yes "" | sudo add-apt-repository ppa:moka/daily
  yes "" | sudo add-apt-repository ppa:snwh/pulp

  # Refresh repository
  echo -n "running apt-get update..." && sudo apt-get update -y -qq && echo " done."

  # Numix Icon Theme and Unity Tweak
  sudo apt-get install -y numix-gtk-theme
  sudo apt-get install -y numix-icon-theme-circle
  sudo apt-get install -y numix-wallpaper-*
  sudo apt-get install -y unity-tweak-tool

  # Moka Icon Theme
  sudo apt-get install -y moka-icon-theme

  # Faba Icon Theme
  sudo apt-get install -y faba-icon-theme
  sudo apt-get install -y faba-mono-icons

  # Paper Icon Theme
  sudo apt-get install -y paper-icon-theme
  sudo apt-get install -y paper-gtk-theme
  sudo apt-get install -y paper-cursor-theme

  sudo apt-get -f install -y
fi
