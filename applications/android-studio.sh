if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

ANDROID_STUDIO=$(ls /opt/ | grep android-studio)
DEB_FILE_NAME="android-studio.tar.gz"
DEB_PATH="/tmp/$DEB_FILE_NAME"
EXIST=$(ls /tmp/ | grep $DEB_FILE_NAME)
APPLICATION_PATH="$(dirname $0)"
sh "$APPLICATION_PATH/java-sdk-8.sh"
CURRENT_USER=$(sudo echo "$HOME" | sed -e "s/\/home\//""/")

ANDROID_STUDIO_DESKTOP="
[Desktop Entry]\n
Version=1.0\n
Type=Application\n
Name=Android Studio\n
Exec=\"/opt/android-studio/bin/studio.sh\" %f\n
Icon=/opt/android-studio/bin/studio.png\n
Categories=Development;IDE;\n
Terminal=false\n
StartupNotify=true\n
StartupWMClass=android-studio
"

if [ -z "$ANDROID_STUDIO" ]; then
  if [ -z "$EXIST" ]; then
    wget -O $DEB_PATH $ANDROID_STUDIO_DOWNLOAD_PATH
  fi

  cd /tmp/
  sudo tar xvfz $DEB_PATH &&
  sudo mv /tmp/android-studio/ /opt/ &&
  sudo chown -R $CURRENT_USER:$CURRENT_USER /opt/ &&

  sudo rm -rf /usr/share/applications/android-studio.desktop ~/.local/share/applications/android-studio.desktop &&

  echo $ANDROID_STUDIO_DESKTOP > /usr/share/applications/android-studio.desktop &&
  echo $ANDROID_STUDIO_DESKTOP > ~/.local/share/applications/android-studio.desktop &&

  sudo update-desktop-database /usr/share/applications/ &&
  sudo update-desktop-database ~/.local/share/applications/
fi
