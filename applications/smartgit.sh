if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

sudo updatedb
SMARTGIT=$(locate smartgit | grep /usr/share/smartgit/bin/smartgit.sh)
DEB_FILE_NAME="smartgit.deb"
EXIST=$(ls /tmp/ | grep $DEB_FILE_NAME)
DEB_PATH="/tmp/$DEB_FILE_NAME"

if [ -z "$SMARTGIT" ]; then
  if [ -z "$EXIST" ]; then
    wget -O $DEB_PATH $SMARTGIT_DOWNLOAD_PATH
  fi

  sudo dpkg -i $DEB_PATH
  sudo apt-get -f install -y
fi
