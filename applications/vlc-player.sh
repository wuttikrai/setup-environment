if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

sudo updatedb
VLC=$(locate vlc | grep /usr/lib/vlc/plugins/)
VLC_PLUGIN=$(locate browser-plugin-vlc | grep /usr/share/doc/browser-plugin-vlc)

if [ -z "$VLC" ]; then
  echo -n "running apt-get update..." && sudo apt-get update -y -qq && echo " done."
  sudo apt-get install -y vlc
fi

if [ -z "$VLC_PLUGIN" ]; then
  sudo apt-get install -y browser-plugin-vlc
fi
