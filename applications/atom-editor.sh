if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

ATOM=$(atom -v | grep -E "^Atom\ .+:.+")
DEB_FILE_NAME="atom-editor.deb"
EXIST=$(ls /tmp/ | grep $DEB_FILE_NAME)
DEB_PATH="/tmp/$DEB_FILE_NAME"

if [ -z "$ATOM" ]; then
  if [ -z "$EXIST" ]; then
    wget -O $DEB_PATH $ATOM_DOWNLOAD_PATH
  fi

  sudo dpkg -i $DEB_PATH
  sudo apt-get -f install -y
fi
