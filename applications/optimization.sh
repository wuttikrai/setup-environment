if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

CURRENT_USER=$(sudo echo "$HOME" | sed -e "s/\/home\//""/")

no_exist() {
  EXIST=$(sudo find /tmp/ -name $1) &&
  if [ -z "$EXIST" ]; then return 0; fi
  return 1
}

copy_file() {
  if no_exist $2; then
    FILE_PATH=$(sudo find $1 -name $2 | head -1) &&
    if [ -n "$FILE_PATH" ]; then
      echo "copying $2 to /tmp/"
      sudo cp "$FILE_PATH" /tmp/
    fi
  fi && check_file $2
}

check_file() {
  cd /tmp/

  if [ -f $1 ]; then
    case $1 in
      *.tar.gz)
        EXIST=$(sudo tar -tvf $1 | head -1) &&
        if [ -z "$EXIST" ]; then sudo rm -rf /tmp/$1; fi
      ;;
      *.deb)
        EXIST=$(sudo dpkg --contents $1 | head -1) &&
        if [ -z "$EXIST" ]; then sudo rm -rf /tmp/$1; fi
      ;;
    esac
  fi
}

verify() {
  copy_file $1 atom-editor.deb
  copy_file $1 google-chrome.deb
  copy_file $1 install-robomongo.tar.gz
  copy_file $1 intellij.tar.gz
  copy_file $1 scala.deb
  copy_file $1 slack.deb
  copy_file $1 smartgit.deb
  copy_file $1 sublime.deb
  copy_file $1 ubuntu-tweak.deb
}

verify /media/*
verify /home/$CURRENT_USER/*
