if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

VIM=$(vim --version | grep -E "VIM\ -\ Vi.+")

if [ -z "$VIM" ]; then
  echo -n "running apt-get update..." && sudo apt-get update -y -qq && echo " done."
  sudo apt-get install -y vim
fi
