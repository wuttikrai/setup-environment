if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

sudo updatedb
SUBLIME=$(subl -v | grep -E "Sublime Text Build")
DEB_FILE_NAME="sublime.deb"
EXIST=$(ls /tmp/ | grep $DEB_FILE_NAME)
DEB_PATH="/tmp/$DEB_FILE_NAME"

if [ -z "$SUBLIME" ]; then
  if [ -z "$EXIST" ]; then
    wget -O $DEB_PATH $SUBLIME_DOWNLOAD_PATH
  fi

  sudo dpkg -i $DEB_PATH
  sudo apt-get -f install -y
fi
