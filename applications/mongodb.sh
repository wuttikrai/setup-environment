if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

MONGODB=$(mongod --version | grep -E "db\ version\ .+")
MONGODB_SERVICE="
[Unit]\n
Description=High-performance, schema-free document-oriented database\n
After=network.target\n
\n
[Service]\n
User=mongodb\n
ExecStart=/usr/bin/mongod --quiet --config /etc/mongod.conf\n
\n
[Install]\n
WantedBy=multi-user.target
"

if [ -z "$MONGODB" ]; then
  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927 &&
  echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list &&
  echo -n "running apt-get update..." && sudo apt-get update -y -qq && echo " done."
  sudo apt-get install -y mongodb-org &&
  echo $MONGODB_SERVICE | sudo tee /etc/systemd/system/mongodb.service &&
  sudo systemctl start mongodb &&
  sudo systemctl status mongodb &&
  sudo systemctl enable mongodb
fi
