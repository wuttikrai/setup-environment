if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

sudo updatedb
DEB_FILE_NAME="ubuntu-tweak.deb"
EXIST=$(ls /tmp/ | grep $DEB_FILE_NAME)
DEB_PATH="/tmp/$DEB_FILE_NAME"
TWEAK=$(locate ubuntu-tweak | grep /usr/bin/ubuntu-tweak)

if [ -z "$TWEAK" ]; then
  if [ -z "$EXIST" ]; then
    wget -O $DEB_PATH $TWEAK_DOWNLOAD_PATH
  fi

  sudo dpkg -i $DEB_PATH
  sudo apt-get -f install -y
fi
