if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

APPLICATION_PATH="$(dirname $0)"
sh "$APPLICATION_PATH/nodejs.sh"
echo -n "running apt-get update..." && sudo apt-get update -y -qq && echo " done."
sudo apt-get install -y build-essential
npm install -g react-native-cli
npm install -g babel
npm install -g babel-cli
