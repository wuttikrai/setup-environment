if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

DOCKER=$(docker --version | grep -E "Docker version.+, build.+")

if [ -z "$DOCKER" ]; then
  CURRENT_USER=$(sudo echo "$HOME" | sed -e "s/\/home\//""/")

  echo -n "running apt-get update..." && sudo apt-get update -y -qq && echo " done."
  sudo apt-get install apt-transport-https ca-certificates -y
  sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
  echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" > /etc/apt/sources.list.d/docker.list

  echo -n "running apt-get update..." && sudo apt-get update -y -qq && echo " done."
  sudo apt-get purge lxc-docker
  sudo apt-cache policy docker-engine

  echo -n "running apt-get update..." && sudo apt-get update -y -qq && echo " done."
  sudo apt-get install docker-engine -y
  sudo service docker start

  sudo groupadd docker
  sudo usermod -aG docker $CURRENT_USER
fi
