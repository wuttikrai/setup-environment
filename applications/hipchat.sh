if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

HIPCHAT=$(hipchat4 -v | grep -E "^HipChat.*")
DEB_FILE_NAME="hipchat.deb"
EXIST=$(ls /tmp/ | grep $DEB_FILE_NAME)
DEB_PATH="/tmp/$DEB_FILE_NAME"

if [ -z "$HIPCHAT" ]; then
  if [ -z "$EXIST" ]; then
    wget -O $DEB_PATH $HIPCHAT_DOWNLOAD_PATH
  fi

  sudo dpkg -i $DEB_PATH
  sudo apt-get -f install -y
fi
