if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

CHROME=$(google-chrome --version | grep -E "^Google\ Chrome\ .+")
DEB_FILE_NAME="google-chrome.deb"
EXIST=$(ls /tmp/ | grep $DEB_FILE_NAME)
DEB_PATH="/tmp/$DEB_FILE_NAME"

if [ -z "$CHROME" ]; then
  if [ -z "$EXIST" ]; then
    wget -O $DEB_PATH "https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"
  fi

  sudo dpkg -i $DEB_PATH
  sudo apt-get -f install -y
fi
