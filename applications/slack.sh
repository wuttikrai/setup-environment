if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

SLACK=$(slack --version)
DEB_FILE_NAME="slack.deb"
EXIST=$(ls /tmp/ | grep $DEB_FILE_NAME)
DEB_PATH="/tmp/$DEB_FILE_NAME"

if [ -z "$SLACK" ]; then
  if [ -z "$EXIST" ]; then
    wget -O $DEB_PATH $SLACK_DOWNLOAD_PATH
  fi

  sudo dpkg -i $DEB_PATH
  sudo apt-get -f install -y
fi
