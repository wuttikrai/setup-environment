if [ $(id -u) -ne 0 ]; then echo "Please run as root"; exit 1; fi

INTELLIJ=$(ls /opt/ | grep intellij)
DEB_FILE_NAME="intellij.tar.gz"
DEB_PATH="/tmp/$DEB_FILE_NAME"
EXIST=$(ls /tmp/ | grep $DEB_FILE_NAME)
CURRENT_USER=$(sudo echo "$HOME" | sed -e "s/\/home\//""/")

INTELLIJ_DESKTOP="
[Desktop Entry]\n
Name=IntelliJ IDEA 2016\n
Comment=Java IDE\n
Exec=/opt/intellij/bin/idea.sh\n
Terminal=false\n
Type=Application\n
StartupNotify=true\n
Icon=/opt/intellij/bin/idea.png\n
Categories=Development;IDE;Debugger;Profiling;
"

if [ -z "$INTELLIJ" ]; then
  if [ -z "$EXIST" ]; then
    wget -O $DEB_PATH $INTELLIJ_DOWNLOAD_PATH
  fi

  cd /tmp/
  sudo rm -rf /tmp/intellij/ /opt/intellij/ &&
  sudo chown -R $CURRENT_USER:$CURRENT_USER $DEB_PATH &&
  sudo tar xvfz $DEB_PATH &&
  sudo mv /tmp/idea-I* /tmp/intellij/ &&
  sudo mv /tmp/intellij/ /opt/ &&
  sudo chown -R $CURRENT_USER:$CURRENT_USER /opt/ &&

  sudo rm -rf /usr/share/applications/intellij.desktop ~/.local/share/applications/intellij.desktop &&

  echo $INTELLIJ_DESKTOP > /usr/share/applications/intellij.desktop &&
  echo $INTELLIJ_DESKTOP > ~/.local/share/applications/intellij.desktop &&

  sudo update-desktop-database /usr/share/applications/ &&
  sudo update-desktop-database ~/.local/share/applications/
fi
